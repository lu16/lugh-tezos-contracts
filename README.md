# Smart contracts for Lugh processing

This repository contains the Lugh-coin contract and the MultiSigs Owner, Administrator, Minter & Reserve to operate Lugh entrypoints.

Each smart contract are specified in a `specs.md`

All the associated tests are listed in a `unit_tests.md` file and archived in a tar.gz

## Global structuration

![Structure](img/structure.png)

## Lugh smart contract

Tezos Smart contract for Lugh stable coin can be found in `Lugh-coin` folder

## MultiSig Owner

Tezos MultiSig Smart contract for Owner operations on Lugh stable coin can be found in `owner` folder

## MultiSig Administrator

Tezos MultiSig Smart contract for Administrator operations on Lugh stable coin can be found in `administrator` folder

## MultiSig Minter

Tezos MultiSig Smart contract for Minter operations on Lugh stable coin can be found in `minter` folder

## MultiSig Reserve

Tezos MultiSig Smart contract for Reserve operations on Lugh stable coin can be found in `reserve` folder

## Lugh operation sequence

### Create proposal

For this illusatration, we focus on a mint proposal from a Lugh platform operator holding an account for signing Minter operations.

![Create proposal](img/create_proposal.png)

### Accept proposal

A Lugh controller selects the mint proposal and sends its approval from its account using the platform.

![Accept proposal](img/accept_proposal.png)

### Reject proposal

A Lugh controller selects the mint proposal and sends its reject from its account using the platform.

![Reject proposal](img/reject_proposal.png)

## SmartPy IDE

To access online [SmartPy IDE](https://smartpy.io/dev/)

Lugh SmartPy based contract can be found in `Lugh-coin/lugh_coin.py`. Execute to compile contract to Michelson and launch test protocols.
