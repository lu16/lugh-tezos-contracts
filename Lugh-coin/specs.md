## INIT

owner address / administrator address / minter address / reserve address

## STORAGE

administrator address / balances bigmap record / circulatingSupply / minter address / owner address / pause bool / reserve address

## ENTRYPOINTS

|    ENTRYPOINT    |               PARAMS                |           PERMISSION            |                                   CONDITIONS                                    |
|------------------|:-----------------------------------:|:-------------------------------:|---------------------------------------------------------------------------------|
| setAdministrator | address                             | owner                           | -                                                                                                |
| setMinter        | address                             | owner                           | -                                                                                                |
| setReserve        | address                             | owner                           | -                                                                                                |
| setPause         | bool                                | admin                           | -                                                                                                |
| setLock          | address: address, lock: bool        | admin                           | -                                                                                                |
| setWhiteListing  | address: address, white: bool       | admin                           | -                                                                                                |
| transfer         | f: address, t: address, amount: int | white listed & unlocked address (except Reserve) or admin | f/sender/t white listed, f unlocked, is not paused, amount =< balance |
| mint             | amount: int       | minter                          | -                                                                          |
| burn             | amount: int       | minter                          | -                                                                                                |

## ERROR CODE

| ERROR CODE | MESSAGE                                    |
|------------|--------------------------------------------|
| #lg01        | Contract is paused                       |
| #lg02        | Only admin allowed                       |
| #lg03        | Only owner allowed                       |
| #lg04        | Only minter allowed                      |
| #lg05        | Address is not referenced                |
| #lg06        | Address is locked                        |
| #lg07        | Address is not white listed              |
| #lg08        | Not allowed to transfer for Reserve      |
| #lg09        | Not allowed to transfer for this address |
| #lg10        | Insufficient balance                     |
| #lg11        | Insufficient balance for reserve         |
| #lg12        | Invalid minting                          |
