import smartpy as sp

class LughCoin(sp.Contract):
    def __init__(self, admin, minter, owner, reserve):
        self.init_type(t = sp.TRecord(administrator = sp.TAddress, balances = sp.TBigMap(sp.TAddress, sp.TRecord(balance = sp.TInt, lock = sp.TBool, white = sp.TBool)), circulatingSupply = sp.TInt, minter =  sp.TAddress, owner = sp.TAddress, paused = sp.TBool, reserve = sp.TAddress))
        self.init(administrator = admin, balances = sp.big_map(), circulatingSupply = 0, minter = minter, owner = owner, paused = True, reserve = reserve)
        
    '''
    Verification utils
    '''
    
    def checkPause(self):
        sp.verify(~self.data.paused, message = "lg01") 
        
    def onlyAdmin(self):
        sp.verify((sp.sender == self.data.administrator), message = "lg02") 
        
    def onlyOwner(self):
        sp.verify((sp.sender == self.data.owner), message = "lg03") 
        
    def onlyMinter(self):
        sp.verify((sp.sender == self.data.minter), message = "lg04") 
        
    def checkSender(self, address):
        sp.verify(self.data.balances.contains(address), message = "lg05")  
        
    def checkUnlock(self, address):
        sp.verify(~self.data.balances[address].lock, message = "lg06")  
        
    def checkWhitelisting(self, address):
        sp.verify((self.data.balances[address].white) | (address == self.data.reserve), message = "lg07")  
        
    def checkTransfer(self, params):
        sp.if sp.sender == self.data.administrator:
            sp.verify(params.f != self.data.reserve, message = "lg08")
            self.checkSender(params.f)
        sp.else:
            self.checkPause()
            self.checkSender(params.f)
            self.checkWhitelisting(params.f)
            self.checkUnlock(params.f)
            self.checkWhitelisting(params.t)
            sp.verify(params.f == sp.sender, message = "lg09")

    def addAddressIfNecessary(self, address):
        sp.if ~ self.data.balances.contains(address):
            self.data.balances[address] = sp.record(balance = 0, lock = False, white = False)

    '''
    Main entrypoints
    '''

    @sp.entry_point
    def setWhiteListing(self, params):
        self.onlyAdmin()
        self.addAddressIfNecessary(params.address)
        self.data.balances[params.address].white = params.white
    
    @sp.entry_point
    def setLock(self, params):
        self.onlyAdmin()
        self.addAddressIfNecessary(params.address)
        self.data.balances[params.address].lock = params.lock

    @sp.entry_point
    def setPause(self, params):
        self.onlyAdmin()
        self.data.paused = params

    @sp.entry_point
    def setReserve(self, params):
        self.onlyOwner()
        self.data.reserve = params

    @sp.entry_point
    def setAdministrator(self, params):
        self.onlyOwner()
        self.data.administrator = params

    @sp.entry_point
    def setMinter(self, params):
        self.onlyOwner()
        self.data.minter = params

    @sp.entry_point
    def transfer(self, params):
        self.checkTransfer(params)
        sp.verify((params.amount > 0) & (self.data.balances[params.f].balance >= params.amount), message = "lg10")
        self.data.balances[params.f].balance -= params.amount
        self.data.balances[params.t].balance += params.amount

    @sp.entry_point
    def burn(self, params):
        self.onlyMinter()
        sp.verify((params > 0) & (self.data.balances[self.data.reserve].balance >= params), message = "lg11")
        self.data.balances[self.data.reserve].balance -= params
        self.data.circulatingSupply -= params

    @sp.entry_point
    def mint(self, params):
        self.onlyMinter()
        self.addAddressIfNecessary(self.data.reserve)
        sp.verify(params > 0, message = "lg12")
        self.data.balances[self.data.reserve].balance += params
        self.data.circulatingSupply += params

    @sp.entry_point
    def getBalance(self, params):
        sp.transfer(sp.as_nat(self.data.balances[params.arg.owner].balance), sp.tez(0), sp.contract(sp.TNat, params.target).open_some())

    @sp.entry_point
    def getCirculatingSupply(self, params):
        sp.transfer(sp.as_nat(self.data.circulatingSupply), sp.tez(0), sp.contract(sp.TNat, params.target).open_some())

    @sp.entry_point
    def getAdministrator(self, params):
        sp.transfer(self.data.administrator, sp.tez(0), sp.contract(sp.TAddress, params.target).open_some())

    @sp.entry_point
    def getMinter(self, params):
        sp.transfer(self.data.minter, sp.tez(0), sp.contract(sp.TAddress, params.target).open_some())

    @sp.entry_point
    def getOwner(self, params):
        sp.transfer(self.data.owner, sp.tez(0), sp.contract(sp.TAddress, params.target).open_some())

if "templates" not in __name__:
    @sp.add_test(name = "LughCoin")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("LughCoin Contract")

        # sp.test_account generates ED25519 key-pairs
        admin = sp.test_account("Administrator")
        owner = sp.test_account("Owner")
        minter = sp.test_account("Minter")
        reserve = sp.test_account("Reserve")
        alice = sp.test_account("Alice")
        bob   = sp.test_account("Bob")
        new_admin = sp.test_account("newAdministrator")
        new_minter = sp.test_account("newMinter")
        new_reserve = sp.test_account("newReserve")

        scenario.h2("Accounts")
        scenario.show([admin, owner, minter, reserve, alice, bob, new_admin, new_minter, new_reserve])

        c1 = LughCoin(admin = admin.address, minter = minter.address, owner = owner.address, reserve = reserve.address)

        scenario += c1
        
        scenario.h2("#lg01 - Minter tries to whiteList Alice's address")
        scenario += c1.setWhiteListing(address = alice.address, white = True).run(sender = minter, valid = False)
        scenario.h2("#lg02 - Administrator whiteLists Alice's address")
        scenario += c1.setWhiteListing(address = alice.address, white = True).run(sender = admin)
        scenario.h2("#lg03 - Administrator whiteLists Alice's address")
        scenario += c1.setWhiteListing(address = alice.address, white = True).run(sender = admin)
        scenario.h2("#lg04 - Administrator removes Alice's address from whiteList")
        scenario += c1.setWhiteListing(address = alice.address, white = False).run(sender = admin)
        scenario.h2("#lg05 - Administrator tries to mint 1000.00 Lugh coins")
        scenario += c1.mint(100000).run(sender = admin, valid = False)
        scenario.h2("#lg06 - Minter mints 1000.00 Lugh coins")
        scenario += c1.mint(100000).run(sender = minter)
        scenario.h2("#lg07 - Minter tries to resume transfers")
        scenario += c1.setPause(False).run(sender = minter, valid = False)
        scenario.h2("#lg08 - Administrator resumes transfers")
        scenario += c1.setPause(False).run(sender = admin)
        scenario.h2("#lg09 - Reserve tries to transfer 200.00 Lugh coins to Alice")
        scenario += c1.transfer(f = reserve.address, t = alice.address, amount = 20000).run(sender = reserve, valid = False)
        scenario.h2("#lg10 - Administrator whiteLists Alice's address")
        scenario += c1.setWhiteListing(address = alice.address, white = True).run(sender = admin)
        scenario.h2("#lg11 - Reserve transfers 200.00 Lugh coins to Alice")
        scenario += c1.transfer(f = reserve.address, t = alice.address, amount = 20000).run(sender = reserve)
        scenario.h2("#lg12 - Administrator whiteLists Bob's address")
        scenario += c1.setWhiteListing(address = bob.address, white = True).run(sender = admin)
        scenario.h2("#lg13 - Alice transfers 50.00 Lugh coins to Bob")
        scenario += c1.transfer(f = alice.address, t = bob.address, amount = 5000).run(sender = alice)
        scenario.h2("#lg14 - Bob tries to transfer 150.00 Lugh coins for Alice to Reserve")
        scenario += c1.transfer(f = alice.address, t = reserve.address, amount = 15000).run(sender = bob, valid = False)
        scenario.h2("#lg15 - Alice transfers 150.00 Lugh coins to Reserve")
        scenario += c1.transfer(f = alice.address, t = reserve.address, amount = 15000).run(sender = alice)
        scenario.h2("#lg16 - Administrator pauses transfers")
        scenario += c1.setPause(True).run(sender = admin)
        scenario.h2("#lg17 - Reserve tries to transfer 200.00 Lugh coins to Alice")
        scenario += c1.transfer(f = reserve.address, t = alice.address, amount = 20000).run(sender = reserve, valid = False)
        scenario.h2("#lg18 - Administrator resumes transfers")
        scenario += c1.setPause(False).run(sender = admin)
        scenario.h2("#lg19 - Administrator tries to force Bob to over transfer to Reserve")
        scenario += c1.transfer(f = bob.address, t = reserve.address, amount = 6000).run(sender = admin, valid = False)
        scenario.h2("#lg20 - Administrator forces Bob to transfer 40.00 Lugh coins to Reserve")
        scenario += c1.transfer(f = bob.address, t = reserve.address, amount = 4000).run(sender = admin)
        scenario.h2("#lg21 - Administrator tries to transfer for Reserve 40.00 Lugh coins to Bob")
        scenario += c1.transfer(f = bob.address, t = reserve.address, amount = 4000).run(sender = admin, valid=False)
        scenario.h2("#lg22 - Bob tries to over transfer to Alice")
        scenario += c1.transfer(f = bob.address, t = alice.address, amount = 10001).run(sender = bob, valid = False)
        scenario.h2("#lg23 - Minter tries to lock Alice address")
        scenario += c1.setLock(address = alice.address, lock = True).run(sender = minter, valid = False)
        scenario.h2("#lg24 - Administrator locks Alice's address")
        scenario += c1.setLock(address = alice.address, lock = True).run(sender = admin)
        scenario.h2("#lg25 - Reserve transfers 200.00 Lugh coins to Alice")
        scenario += c1.transfer(f = reserve.address, t = alice.address, amount = 20000).run(sender = reserve)
        scenario.h2("#lg26 - Alice tries to transfer to 50.00 Lugh coins to Bob")
        scenario += c1.transfer(f = alice.address, t = bob.address, amount = 5000).run(sender = alice, valid = False)
        scenario.h2("#lg27 - Administrator tries to burn 500.00 Lugh coins")
        scenario += c1.burn(50000).run(sender = admin, valid = False)
        scenario.h2("#lg28 - Minter burns 500.00 Lugh coins")
        scenario += c1.burn(50000).run(sender = minter)
        scenario.h2("#lg29 - Administrator tries to change Minter")
        scenario += c1.setMinter(new_minter.address).run(sender = minter, valid = False)
        scenario.h2("#lg30 - Owner changes Minter")
        scenario += c1.setMinter(new_minter.address).run(sender = owner)
        scenario.h2("#lg31 - Minter tries to mint 500.00 Lugh coins")
        scenario += c1.mint(50000).run(sender = minter, valid = False)
        scenario.h2("#lg32 - newMinter mint 500.00 Lugh coins")
        scenario += c1.mint(50000).run(sender = new_minter)
        scenario.h2("#lg33 - Administrator tries to change Reserve")
        scenario += c1.setReserve(new_reserve.address).run(sender = admin, valid = False)
        scenario.h2("#lg34 - Owner changes Reserve")
        scenario += c1.setReserve(new_reserve.address).run(sender = owner)
        scenario.h2("#lg35 - newMinter mint 1000.00 Lugh coins")
        scenario += c1.mint(100000).run(sender = new_minter)
        scenario.h2("#lg36 - Administrator pauses transfers")
        scenario += c1.setPause(True).run(sender = admin)
        scenario.h2("#lg37 - Administrator transfers for Reserve 790.00 Lugh coins to newReserve")
        scenario += c1.transfer(f = reserve.address, t = new_reserve.address, amount = 79000).run(sender = admin)
        scenario.h2("#lg38 - Administrator tries to change Administrator")
        scenario += c1.setAdministrator(bob.address).run(sender = admin, valid = False)
        scenario.h2("#lg39 - Owner changes Administrator")
        scenario += c1.setAdministrator(new_admin.address).run(sender = owner)
        scenario.h2("#lg40 - Administrator tries to resume transfer")
        scenario += c1.setPause(False).run(sender = admin, valid = False)
        scenario.h2("#lg41 - newAdministrator resumes transfer")
        scenario += c1.setPause(False).run(sender = new_admin)
        
        scenario.h2("#lg42 - newMinter tries to over burn from reserve")
        scenario += c1.burn(179001).run(sender = new_minter, valid = False)
        
        scenario.h2("#lg43 - newReserve tries to over transfer")
        scenario += c1.transfer(f = new_reserve.address, t = alice.address, amount = 200000).run(sender = new_minter, valid = False)
        
        scenario.h2("#lg44 - newMinter burns all from reserve")
        scenario += c1.burn(179000).run(sender = new_minter)
        
        scenario.verify(c1.data.circulatingSupply == 21000)
        scenario.verify(c1.data.balances[alice.address].balance == 20000)
        scenario.verify(c1.data.balances[bob.address].balance == 1000)
        scenario.verify(c1.data.balances[new_reserve.address].balance == 0)
