# Multisig owner

Multi signature "setAdministrator, setMinter & setReserve" with acceptation rule all signers

## INIT

signers address set 


## STORAGE

signers set / proposals bigmap / authOps set ("setMinter", "setAdministrator", "setReserve")

## ENTRYPOINTS

| ENTRYPOINT | PARAMS | PERMISSIONS | CONDITIONS |
| ------ | ------ | ------ | ------ |
| createProposal | proposalId: string, operation: string, address: address, contractAddr: address | signer | proposalId does not exist |
| accept | string | signer | proposalId exists and not closed, and not already accepted by caller |
| reject | string | signer | proposalId exists and not closed, and not already rejected by caller |

## ERROR CODE

| ERROR CODE | MESSAGE                              |
|------------|--------------------------------------|
| #01        | Unreferenced signing address         |
| #02        | Proposal Id already referenced       |
| #03        | Proposal Id not referenced           |
| #04        | Open proposals limit reached         |
| #05        | Proposal already closed              |
| #06        | Operation not permitted              |
| #07        | Proposal already accepted            |

