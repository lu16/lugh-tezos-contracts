import smartpy as sp

class OwnerMultiSig(sp.Contract):
    def __init__(self, signers):
        self.init_type(t = sp.TRecord(signers = sp.TSet(sp.TAddress), proposals = sp.TBigMap(sp.TString, sp.TRecord(address = sp.TAddress, operation = sp.TString, contractAddr = sp.TAddress, approvals = sp.TSet(sp.TAddress), rejects = sp.TSet(sp.TAddress), status = sp.TBool)), authOps = sp.TSet(sp.TString), restriction =  sp.TInt, openProposals = sp.TInt))
        self.init(signers = signers, proposals = sp.big_map(), authOps = {"setMinter", "setAdministrator", "setReserve"}, restriction = 3, openProposals = 0)
        
    '''
    Verification utils
    '''
    
    def checkSigner(self):
        sp.verify(self.data.signers.contains(sp.sender), message = "01")
    
    def checkNewProposal(self, proposalId):
        sp.verify(~self.data.proposals.contains(proposalId), message = "02")
    
    def checkProposal(self, proposalId):
        sp.verify(self.data.proposals.contains(proposalId), message = "03")
        
    def checkRestriction(self):
        sp.verify(self.data.restriction > self.data.openProposals, message = "04")
    
    def checkOpen(self, proposalId):
        sp.verify(~self.data.proposals[proposalId].status, message = "05")
    
    def checkOperation(self, operation):
        sp.verify(self.data.authOps.contains(operation), message = "06")
        
    '''
    Main entrypoints
    '''

    @sp.entry_point
    def createProposal(self, params):
        self.checkSigner()
        self.checkRestriction()
        self.checkOperation(params.operation)
        self.checkNewProposal(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(address = params.address, operation = params.operation, contractAddr = params.contractAddr, approvals = sp.set(t = sp.TAddress), rejects = sp.set(t = sp.TAddress), status = False)
        self.data.proposals[params.proposalId].approvals.add(sp.sender)
        self.data.openProposals += 1

    @sp.entry_point
    def accept(self, params):
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        sp.verify(~self.data.proposals[params].approvals.contains(sp.sender), message = "07")
        self.data.proposals[params].approvals.add(sp.sender)
        sp.if sp.len(self.data.proposals[params].approvals) == sp.len(self.data.signers):
            sp.if self.data.proposals[params].operation == "setMinter":
                self._processOperation(params, "setMinter")
            sp.else:
                sp.if self.data.proposals[params].operation == "setAdministrator":
                    self._processOperation(params, "setAdministrator")
                sp.else:
                    sp.if self.data.proposals[params].operation == "setReserve":
                        self._processOperation(params, "setReserve")
            self.data.proposals[params].status = True
            self.data.openProposals -= 1

    @sp.entry_point
    def reject(self, params):
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        self.data.proposals[params].rejects.add(sp.sender)
        sp.if self.data.proposals[params].approvals.contains(sp.sender):
            self.data.proposals[params].approvals.remove(sp.sender)
        self.data.proposals[params].status = True
        self.data.openProposals -= 1
        
    '''
    Lugh coin operations
    '''
    
    def _processOperation(self, proposalId, ePoint):
        transferParamsRecord = sp.record(address = self.data.proposals[proposalId].address)
        c = sp.contract(
                t = sp.TRecord(address = sp.TAddress), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = ePoint
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)

if "templates" not in __name__:
    @sp.add_test(name = "MultiSig")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("OwnerMultiSig Contract")

        # sp.test_account generates ED25519 key-pairs deterministically:
        firstSigner = sp.test_account("firstSigner")
        secondSigner = sp.test_account("secondSigner")
        thirdSigner = sp.test_account("thirdSigner")
        new_minter = sp.test_account("newMinter")
        new_administrator = sp.test_account("newAdministrator")
        new_reserve = sp.test_account("newReserve")
        contractAddr = sp.address("KT1-distantContractToCall-1234")

        # Let's display the accounts:
        scenario.h2("Accounts")
        scenario.show([firstSigner, secondSigner, thirdSigner, new_minter, new_administrator, new_reserve])

        c1 = OwnerMultiSig(signers = sp.set([firstSigner.address, secondSigner.address, thirdSigner.address]))

        scenario += c1
        
        scenario.h2("#o01 - firstSigner tries to create a transfer proposal")
        scenario += c1.createProposal(proposalId = "om01", address = new_minter.address, operation = "setOwner", contractAddr = contractAddr).run(sender = firstSigner, valid = False)
        scenario.h2("#o02 - firstSigner creates a setMinter proposal")
        scenario += c1.createProposal(proposalId = "om01", address = new_minter.address, operation = "setMinter", contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#o03 - firstSigner accepts proposal")
        scenario += c1.accept("om01").run(sender = firstSigner, valid=False)
        scenario.h2("#o04 - secondSigner accepts unreferenced proposal")
        scenario += c1.accept("om02").run(sender = secondSigner, valid=False)
        scenario.h2("#o05 - secondSigner accepts proposal")
        scenario += c1.accept("om01").run(sender = secondSigner)
        scenario.h2("#o06 - thirdSigner accepts proposal")
        scenario += c1.accept("om01").run(sender = thirdSigner)
        scenario.h2("#o07 - secondSigner tries to create a setAdministrator proposal")
        scenario += c1.createProposal(proposalId = "om01", address = new_administrator.address, operation = "setAdministrator", contractAddr = contractAddr).run(sender = secondSigner, valid = False)
        scenario.h2("#o08 - secondSigner creates a setAdministrator proposal")
        scenario += c1.createProposal(proposalId = "oa01", address = new_administrator.address, operation = "setAdministrator", contractAddr = contractAddr).run(sender = secondSigner)
        scenario.h2("#o09 - firstSigner rejects proposal")
        scenario += c1.reject("oa01").run(sender = firstSigner)
        scenario.h2("#o10 - thirdSigner accepts proposal")
        scenario += c1.accept("oa01").run(sender = thirdSigner, valid=False)
        scenario.h2("#o11 - firstSigner accepts proposal")
        scenario += c1.accept("oa01").run(sender = firstSigner, valid=False)
        scenario.h2("#o12 - thirdSigner creates a setReserve proposal")
        scenario += c1.createProposal(proposalId = "or01", address = new_reserve.address, operation = "setReserve", contractAddr = contractAddr).run(sender = thirdSigner)
        scenario.h2("#o13 - firstSigner accepts proposal")
        scenario += c1.accept("or01").run(sender = firstSigner)
        scenario.h2("#o14 - secondSigner accepts proposal")
        scenario += c1.accept("or01").run(sender = secondSigner)
        scenario.h2("#o15 - firstSigner creates a setMinter proposal")
        scenario += c1.createProposal(proposalId = "om02", address = new_administrator.address, operation = "setMinter", contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#o16 - secondSigner rejects proposal")
        scenario += c1.reject("om02").run(sender = secondSigner)
        scenario.h2("#o17 - thirdSigner rejects proposal")
        scenario += c1.reject("om02").run(sender = thirdSigner, valid=False)
        scenario.h2("#o18 - firstSigner rejects proposal")
        scenario += c1.reject("om02").run(sender = firstSigner, valid=False)
        
        scenario.h1("Tests overflowding")
        scenario.h2("#o19 - firstSigner creates a first setMinter proposal")
        scenario += c1.createProposal(proposalId = "over01", address = new_minter.address, operation = "setMinter", contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#o20 - firstSigner creates a second setMinter proposal")
        scenario += c1.createProposal(proposalId = "over02", address = new_minter.address, operation = "setMinter", contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#o21 - firstSigner creates a third setMinter proposal")
        scenario += c1.createProposal(proposalId = "over03", address = new_minter.address, operation = "setMinter", contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#o22 - firstSigner creates a fourth setMinter proposal")
        scenario += c1.createProposal(proposalId = "over04", address = new_minter.address, operation = "setMinter", contractAddr = contractAddr).run(sender = firstSigner, valid = False)
        
