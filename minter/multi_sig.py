import smartpy as sp

class MinterMultiSig(sp.Contract):
    def __init__(self, controllers, operators):
        self.init_type(t = sp.TRecord(controllers = sp.TSet(sp.TAddress), operators = sp.TSet(sp.TAddress), proposals = sp.TBigMap(sp.TString, sp.TRecord(amount = sp.TInt, operation = sp.TString, contractAddr = sp.TAddress, opApprovals = sp.TSet(sp.TAddress), opRejects = sp.TSet(sp.TAddress), ctrlApprovals = sp.TSet(sp.TAddress), ctrlRejects = sp.TSet(sp.TAddress), status = sp.TBool)), authOps = sp.TSet(sp.TString), limit =  sp.TNat, restriction =  sp.TInt, openProposals = sp.TInt))
        self.init(controllers = controllers, operators = operators, proposals = sp.big_map(), authOps = {"mint", "burn"}, limit = 1, restriction = 10, openProposals = 0)
        
    '''
    Verification utils
    '''
    
    def checkSigner(self):
        sp.verify(self.data.operators.contains(sp.sender) | self.data.controllers.contains(sp.sender), message = "01")
    
    def checkNewProposal(self, proposalId):
        sp.verify(~self.data.proposals.contains(proposalId), message = "02")
    
    def checkProposal(self, proposalId):
        sp.verify(self.data.proposals.contains(proposalId), message = "03")
        
    def checkRestriction(self):
        sp.verify(self.data.restriction > self.data.openProposals, message = "04")
    
    def checkOpen(self, proposalId):
        sp.verify(~self.data.proposals[proposalId].status, message = "05")
    
    def checkOperation(self, operation):
        sp.verify(self.data.authOps.contains(operation), message = "06")
        
    '''
    Main entrypoints
    '''

    @sp.entry_point
    def createProposal(self, params):
        self.checkSigner()
        self.checkOperation(params.operation)
        self.checkRestriction()
        sp.verify(params.amount > 0, message = "07")
        self.checkNewProposal(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(amount = params.amount, operation = params.operation, contractAddr = params.contractAddr, opApprovals = sp.set(t = sp.TAddress), opRejects = sp.set(t = sp.TAddress), ctrlApprovals = sp.set(t = sp.TAddress), ctrlRejects = sp.set(t = sp.TAddress), status = False)
        sp.if self.data.operators.contains(sp.sender):
            self.data.proposals[params.proposalId].opApprovals.add(sp.sender)
        sp.else:
            self.data.proposals[params.proposalId].ctrlApprovals.add(sp.sender)
        self.data.openProposals += 1

    @sp.entry_point
    def accept(self, params):
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        sp.if self.data.operators.contains(sp.sender):
            sp.verify(~self.data.proposals[params].opApprovals.contains(sp.sender), message = "08")
            self.data.proposals[params].opApprovals.add(sp.sender)
        sp.else:
            sp.verify(~self.data.proposals[params].ctrlApprovals.contains(sp.sender), message = "08")
            self.data.proposals[params].ctrlApprovals.add(sp.sender)
        sp.if ((sp.len(self.data.proposals[params].opApprovals) >= self.data.limit) & (sp.len(self.data.proposals[params].ctrlApprovals) >= self.data.limit)):
            sp.if self.data.proposals[params].operation == "mint":
                self._processOperation(params, "mint")
            sp.else:
                self._processOperation(params, "burn")
            self.data.proposals[params].status = True
            self.data.openProposals -= 1

    @sp.entry_point
    def reject(self, params):
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        sp.if self.data.operators.contains(sp.sender):
            self.data.proposals[params].opRejects.add(sp.sender)
            sp.if self.data.proposals[params].opApprovals.contains(sp.sender):
                self.data.proposals[params].opApprovals.remove(sp.sender)
        sp.else:
            self.data.proposals[params].ctrlRejects.add(sp.sender)
            sp.if self.data.proposals[params].ctrlApprovals.contains(sp.sender):
                self.data.proposals[params].ctrlApprovals.remove(sp.sender)
        self.data.proposals[params].status = True
        self.data.openProposals -= 1
        
    '''
    Lugh coin operations
    '''
    
    def _processOperation(self, proposalId, ePoint):
        transferParamsRecord = sp.record(amount = self.data.proposals[proposalId].amount)
        c = sp.contract(
                t = sp.TRecord(amount = sp.TIntOrNat), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = ePoint
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)

if "templates" not in __name__:
    @sp.add_test(name = "MinterMultiSig")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("MinterMultiSig Contract")

        # sp.test_account generates ED25519 key-pairs deterministically:
        opFirstSigner = sp.test_account("opFirstSigner")
        opSecondSigner = sp.test_account("opSecondSigner")
        ctrlFirstSigner = sp.test_account("ctrlFirstSigner")
        ctrlSecondSigner = sp.test_account("ctrlSecondSigner")
        contractAddr = sp.address("KT1-distantContractToCall-1234")

        # Let's display the accounts:
        scenario.h2("Accounts")
        scenario.show([opFirstSigner, opSecondSigner, ctrlFirstSigner, ctrlSecondSigner])

        c1 = MinterMultiSig(operators = sp.set([opFirstSigner.address, opSecondSigner.address]), controllers = sp.set([ctrlFirstSigner.address, ctrlSecondSigner.address]))

        scenario += c1
        
        scenario.h2("#m01 - opFirstSigner tries to create a transfer proposal")
        scenario += c1.createProposal(proposalId = "mm01", amount = 1000, operation = "transfer", contractAddr = contractAddr).run(sender = opFirstSigner, valid = False)
        scenario.h2("#m02 - opFirstSigner creates a mint proposal")
        scenario += c1.createProposal(proposalId = "mm01", amount = 1000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner)
        scenario.h2("#m03 - opFirstSigner accepts proposal")
        scenario += c1.accept("mm01").run(sender = opFirstSigner, valid=False)
        scenario.h2("#m04 - ctrlSecondSigner accepts unreferenced proposalId")
        scenario += c1.accept("mm02").run(sender = ctrlSecondSigner, valid=False)
        scenario.h2("#m05 - opSecondSigner accepts proposal")
        scenario += c1.accept("mm01").run(sender = opSecondSigner)
        scenario.h2("#m06 - ctrlSecondSigner accepts proposal")
        scenario += c1.accept("mm01").run(sender = ctrlSecondSigner)
        scenario.h2("#m07 - ctrlFirstSigner accepts proposal")
        scenario += c1.accept("mm01").run(sender = ctrlFirstSigner, valid=False)
        
        scenario.h2("#m08 - opSecondSigner creates a burn proposal with referenced proposalId")
        scenario += c1.createProposal(proposalId = "mm01", amount = 200, operation = "burn", contractAddr = contractAddr).run(sender = opSecondSigner, valid=False)
        scenario.h2("#m09 - opSecondSigner creates a burn proposal")
        scenario += c1.createProposal(proposalId = "mb01", amount = 200, operation = "burn", contractAddr = contractAddr).run(sender = opSecondSigner)
        scenario.h2("#m10 - opSecondSigner rejects burn proposal")
        scenario += c1.reject("mb01").run(sender = opSecondSigner)
        scenario.h2("#m11 - opFirstSigner rejects proposal")
        scenario += c1.reject("mb01").run(sender = opFirstSigner, valid=False)
        scenario.h2("#m12 - ctrlFirstSigner rejects proposal")
        scenario += c1.reject("mb01").run(sender = ctrlFirstSigner, valid=False)
        scenario.h2("#m13 - ctrlSecondSigner rejects proposal")
        scenario += c1.reject("mb01").run(sender = ctrlSecondSigner, valid=False)
        scenario.h2("#m14 - opSecondSigner creates a burn proposal")
        scenario += c1.createProposal(proposalId = "mb02", amount = 100, operation = "burn", contractAddr = contractAddr).run(sender = opSecondSigner)
        scenario.h2("#m15 - ctrlSecondSigner accepts proposal")
        scenario += c1.accept("mb02").run(sender = ctrlSecondSigner)
        scenario.h2("#m16 - opSecondSigner creates an over burn proposal")
        scenario += c1.createProposal(proposalId = "mb03", amount = 900000, operation = "burn", contractAddr = contractAddr).run(sender = opSecondSigner)
        scenario.h2("#m17 - ctrlSecondSigner accepts proposal")
        scenario += c1.accept("mb03").run(sender = ctrlSecondSigner)
        
        scenario.h1("Tests overflowding")
        scenario.h2("#m18 - opFirstSigner creates a first mint proposal")
        scenario += c1.createProposal(proposalId = "over01", amount = 10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner)
        scenario.h2("#m19 - opFirstSigner creates a second mint proposal")
        scenario += c1.createProposal(proposalId = "over02", amount = 10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner)
        scenario.h2("#m20 - opFirstSigner creates a third mint proposal")
        scenario += c1.createProposal(proposalId = "over03", amount = 10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner)
        scenario.h2("#m21 - opFirstSigner creates a fourth mint proposal")
        scenario += c1.createProposal(proposalId = "over04", amount = 10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner)
        scenario.h2("#m22 - opFirstSigner creates a fifth mint proposal")
        scenario += c1.createProposal(proposalId = "over05", amount = 10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner)
        scenario.h2("#m23 - opFirstSigner creates a sixth mint proposal (overflowding)")
        scenario += c1.createProposal(proposalId = "over06", amount = 10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner)
        scenario.h2("#m24 - opFirstSigner creates a sixth mint proposal (overflowding)")
        scenario += c1.createProposal(proposalId = "over07", amount = 10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner)
        scenario.h2("#m25 - opFirstSigner creates a sixth mint proposal (overflowding)")
        scenario += c1.createProposal(proposalId = "over08", amount = 10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner)
        scenario.h2("#m26 - opFirstSigner creates a sixth mint proposal (overflowding)")
        scenario += c1.createProposal(proposalId = "over09", amount = 10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner)
        scenario.h2("#m27 - opFirstSigner creates a sixth mint proposal (overflowding)")
        scenario += c1.createProposal(proposalId = "over10", amount = 10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner)
        scenario += c1.createProposal(proposalId = "over11", amount = 10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner, valid=False)
        scenario.h2("#m28 - ctrlFirstSigner rejects proposal over01")
        scenario += c1.reject("over01").run(sender = ctrlFirstSigner)
        scenario.h2("#m29 - opFirstSigner creates a negative mint")
        scenario += c1.createProposal(proposalId = "neg01", amount = -10000, operation = "mint", contractAddr = contractAddr).run(sender = opFirstSigner, valid = False)
