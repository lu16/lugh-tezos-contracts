# Multisig minter

Multi signature "mint & burn" with acceptation rule set to 1 min. signer per college i.e. operators & controllers

## INIT

operators set(address), controllers set(address)


## STORAGE

operators set / controllers set / proposals bigmap / limit int

## ENTRYPOINTS

| ENTRYPOINT | PARAMS | PERMISSIONS | CONDITIONS |
| ------ | ------ | ------ | ------ |
| createProposal | proposalId: string, operation: string, amount: int, contractAddr: address | signer | proposalId does not exist |
| accept | string | signer | proposalId exists and not closed, and not already accepted by caller |
| reject | string | signer | proposalId exists and not closed, and not already rejected by caller |

## ERROR CODE

| ERROR CODE | MESSAGE                              |
|------------|--------------------------------------|
| #01        | Unreferenced signing address         |
| #02        | Proposal Id already referenced       |
| #03        | Proposal Id not referenced           |
| #04        | Open proposals limit reached         |
| #05        | Proposal already closed              |
| #06        | Operation not allowed                |
| #07        | Mint/burn amount not permitted       | 
| #08        | Proposal already accepted            |
