# Multisig Reserve wallet

Multi signature "transfer" with acceptation rule 2 min. signers

## INIT

signers address set 


## STORAGE

signers set / proposals bigmap / limit int

## ENTRYPOINTS

| ENTRYPOINT | PARAMS | PERMISSIONS | CONDITIONS |
| ------ | ------ | ------ | ------ |
| createProposal | proposalId: string, f: address, t: address, amount: int, contractAddr: address | signer | proposalId does not exist |
| accept | string | signer | proposalId exists and not closed, and not already accepted by caller |
| reject | string | signer | proposalId exists and not closed, and not already rejected by caller |

## ERROR CODE

| ERROR CODE | MESSAGE                        |
|------------|--------------------------------|
| #01        | Unreferenced signing address   |
| #02        | Proposal Id already referenced |
| #03        | Proposal Id not referenced     |
| #04        | Open proposals limit reached   |
| #05        | Proposal already closed        |
| #06        | Transfer amount not permitted  |
| #07        | Proposal already accepted      |
