# Init

```bash
$ tezos-client -A carthagenet.smartpy.io -P 443 -S config update
$ tezos-client activate account init with tz1dj1LKb5eRW4KmHLyhQZTGPBiXHJKwPz35.json
$ tezos-client activate account signer1 with w1-tz1asaAAZmAFXPcBWLPna9Ljs1n9jjV5TJQn.json
$ tezos-client activate account signer2 with w2-tz1RDm79HrpwyHt7J8VxTL9aKgwoqNBKof4u.json
$ tezos-client activate account signer3 with w3-tz1aSfJhjSJN5c9kFYYJciKwpRhc74odu12A.json
$ tezos-client activate account signer4 with w4-tz1SKjd4Co95Fm5fgHyViGh9SgEE6Kxb4P7z.json
```

# Originations

Originate multisig_reserve with signer1, signer2, signer3
---------------------------------------------------------

```bash
$ tezos-client originate contract multisig_reserved transferring 0 from init running ../stable-smart-contract/reserve/multi_sig.tz --init '(Pair 2 (Pair {} {"tz1RDm79HrpwyHt7J8VxTL9aKgwoqNBKof4u" ; "tz1aSfJhjSJN5c9kFYYJciKwpRhc74odu12A"; "tz1asaAAZmAFXPcBWLPna9Ljs1n9jjV5TJQn"}))' --burn-cap 3.849
```

Originate multisig_owner signer1, signer2, signer3
--------------------------------------------------

```bash
$ tezos-client originate contract multisig_owner transferring 0 from init running ../stable-smart-contract/owner/multi_sig.tz --init '(Pair {"setAdministrator"; "setMinter"; "setReserve"} (Pair {} {"tz1RDm79HrpwyHt7J8VxTL9aKgwoqNBKof4u";"tz1aSfJhjSJN5c9kFYYJciKwpRhc74odu12A";"tz1asaAAZmAFXPcBWLPna9Ljs1n9jjV5TJQn"}))' --burn-cap 4.532
```

Originate multisig_minter col(signer1, signer2), col(signer3, signer4)
----------------------------------------------------------------------

```bash
$ tezos-client originate contract multisig_minter transferring 0 from init running ../stable-smart-contract/minter/multi_sig.tz --init '(Pair (Pair {"burn"; "mint"} {"tz1RDm79HrpwyHt7J8VxTL9aKgwoqNBKof4u";"tz1asaAAZmAFXPcBWLPna9Ljs1n9jjV5TJQn"}) (Pair 1 (Pair {"tz1SKjd4Co95Fm5fgHyViGh9SgEE6Kxb4P7z"; "tz1aSfJhjSJN5c9kFYYJciKwpRhc74odu12A"}{})))' --burn-cap 6.071
```

Originate multisif_admin signer1, signer2, signer3
--------------------------------------------------

```bash
$ tezos-client originate contract multisig_admin transferring 0 from init running ../stable-smart-contract/administrator/multi_sig.tz --init '(Pair (Pair {} (Pair {"setLock"; "setWhiteListing"} 2)) (Pair {} (Pair {"tz1RDm79HrpwyHt7J8VxTL9aKgwoqNBKof4u"; "tz1aSfJhjSJN5c9kFYYJciKwpRhc74odu12A"; "tz1asaAAZmAFXPcBWLPna9Ljs1n9jjV5TJQn"} {})))' --burn-cap 11.272
```

Originate lughcoin
------------------

```bash
$ tezos-client originate contract lughcoin transferring 0 from init running ../stable-smart-contract/Lugh-coin/lugh_coin.tz --init '(Pair (Pair "KT19sRjAFhVQ7zLqrDbGJxN5b2R6HPbYqVyg" (Pair {} 0)) (Pair (Pair "KT1DYr2Y82U8Z7DqUiFcyZajAN3oUKgEF4CH" "KT1PT56ykt1XF6aoJUSerTtZJHubGziCqr5g") (Pair False "KT1RmNH9fAufV95TUPqzpHKpMeh5qMcSZJAT")))' --burn-cap 4.889
```

Known addresses
---------------

```bash
$ tezos-client list known contracts
lughcoin: KT1Qr2ar6VdvSu6vH4ewMKz4bL5HB23TWQaD
multisig_admin: KT19sRjAFhVQ7zLqrDbGJxN5b2R6HPbYqVyg
multisig_minter: KT1DYr2Y82U8Z7DqUiFcyZajAN3oUKgEF4CH
multisig_owner: KT1PT56ykt1XF6aoJUSerTtZJHubGziCqr5g
multisig_reserved: KT1RmNH9fAufV95TUPqzpHKpMeh5qMcSZJAT
signer4: tz1SKjd4Co95Fm5fgHyViGh9SgEE6Kxb4P7z
signer3: tz1aSfJhjSJN5c9kFYYJciKwpRhc74odu12A
signer2: tz1RDm79HrpwyHt7J8VxTL9aKgwoqNBKof4u
signer1: tz1asaAAZmAFXPcBWLPna9Ljs1n9jjV5TJQn
init: tz1dj1LKb5eRW4KmHLyhQZTGPBiXHJKwPz35
```

# Proposal 

Create mint proposal
--------------------

```bash
$ tezos-client transfer 0 from signer1 to multisig_minter --entrypoint createProposal --arg '(Pair (Pair 1000 "KT1Qr2ar6VdvSu6vH4ewMKz4bL5HB23TWQaD") (Pair "mint" "mt01"))' --burn-cap 0.167
```

Accept proposal by signer3 (already accepted by signer1)
--------------------------------------------------------

```bash
$ tezos-client transfer 0 from signer3 to multisig_minter --entrypoint accept --arg '"mt01"' --burn-cap 0.104
```

Minter proposal approved
------------------------

```bash
$ tezos-client get big map value for '"mt01"' of type string in multisig_minter
Pair (Pair (Pair 1000 "KT1Qr2ar6VdvSu6vH4ewMKz4bL5HB23TWQaD")
           (Pair { "tz1asaAAZmAFXPcBWLPna9Ljs1n9jjV5TJQn" } {}))
     (Pair (Pair { "tz1aSfJhjSJN5c9kFYYJciKwpRhc74odu12A" } {}) (Pair "mint" True))
```

Lughcoin storage (with 1000 circulatin supply)
----------------------------------------------

```bash
$ tezos-client get contract storage for lughcoin
Pair (Pair "KT19sRjAFhVQ7zLqrDbGJxN5b2R6HPbYqVyg" (Pair 11220 1000))
     (Pair (Pair "KT1DYr2Y82U8Z7DqUiFcyZajAN3oUKgEF4CH" "KT1PT56ykt1XF6aoJUSerTtZJHubGziCqr5g")
           (Pair False "KT1RmNH9fAufV95TUPqzpHKpMeh5qMcSZJAT"))
```
