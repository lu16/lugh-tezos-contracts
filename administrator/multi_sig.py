import smartpy as sp

class AdminMultiSig(sp.Contract):
    def __init__(self, signers):
        self.init_type(t = sp.TRecord(signers = sp.TSet(sp.TAddress), proposals = sp.TBigMap(sp.TString, sp.TRecord(f = sp.TAddress, amount = sp.TInt, t = sp.TAddress, state = sp.TBool, operation = sp.TString, contractAddr = sp.TAddress, approvals = sp.TSet(sp.TAddress), rejects = sp.TSet(sp.TAddress), status = sp.TBool)), authOps = sp.TSet(sp.TString), limit =  sp.TNat, restriction =  sp.TInt, openProposals = sp.TInt))
        self.init(signers = signers, proposals = sp.big_map(), authOps = {"setWhiteListing", "setLock"}, limit = 2, restriction = 40, openProposals = 0)
        
    '''
    Verification utils
    '''
    
    def checkSigner(self):
        sp.verify(self.data.signers.contains(sp.sender), message = "01")
    
    def checkNewProposal(self, proposalId):
        sp.verify(~self.data.proposals.contains(proposalId), message = "02")
    
    def checkProposal(self, proposalId):
        sp.verify(self.data.proposals.contains(proposalId), message = "03")
    
    def checkRestriction(self):
        sp.verify(self.data.restriction > self.data.openProposals, message = "04")
    
    def checkOpen(self, proposalId):
        sp.verify(~self.data.proposals[proposalId].status, message = "05")
    
    def checkOperation(self, operation):
        sp.verify(self.data.authOps.contains(operation), message = "06")
        
    '''
    Main entrypoints
    '''

    @sp.entry_point
    def createAddressProposal(self, params):
        self.checkSigner()
        self.checkOperation(params.operation)
        self.checkRestriction()
        self.checkNewProposal(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(f = sp.sender, amount = 0, t = params.address, state = params.state, operation = params.operation, contractAddr = params.contractAddr, approvals = sp.set(t = sp.TAddress), rejects = sp.set(t = sp.TAddress), status = False)
        self.data.proposals[params.proposalId].approvals.add(sp.sender)
        self.data.openProposals += 1

    @sp.entry_point
    def createPauseProposal(self, params):
        self.checkSigner()
        self.checkRestriction()
        self.checkNewProposal(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(f = sp.sender, amount = 0, t = params.contractAddr, state = params.state, operation = "setPause", contractAddr = params.contractAddr, approvals = sp.set(t = sp.TAddress), rejects = sp.set(t = sp.TAddress), status = False)
        self.data.proposals[params.proposalId].approvals.add(sp.sender)
        self.data.openProposals += 1

    @sp.entry_point
    def createTransferProposal(self, params):
        self.checkSigner()
        self.checkRestriction()
        sp.verify(params.amount > 0, message = "07")
        self.checkNewProposal(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(f = params.f, amount = params.amount, t = params.t, contractAddr = params.contractAddr, state = True, operation = "transfer", approvals = sp.set(t = sp.TAddress), rejects = sp.set(t = sp.TAddress), status = False)
        self.data.proposals[params.proposalId].approvals.add(sp.sender)
        self.data.openProposals += 1

    @sp.entry_point
    def accept(self, params):
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        sp.verify(~self.data.proposals[params].approvals.contains(sp.sender), message = "08")
        self.data.proposals[params].approvals.add(sp.sender)
        sp.if sp.len(self.data.proposals[params].approvals) == self.data.limit:
            sp.if self.data.proposals[params].operation == "setWhiteListing":
                self._setWhiteListing(params)
            sp.else:
                sp.if self.data.proposals[params].operation == "setLock":
                    self._setLock(params)
                sp.else:
                    sp.if self.data.proposals[params].operation == "transfer":
                        self._transfer(params)
                    sp.else:
                        self._setPause(params)
            self.data.proposals[params].status = True
            self.data.openProposals -= 1

    @sp.entry_point
    def reject(self, params):
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        self.data.proposals[params].rejects.add(sp.sender)
        sp.if self.data.proposals[params].approvals.contains(sp.sender):
            self.data.proposals[params].approvals.remove(sp.sender)
        self.data.proposals[params].status = True
        self.data.openProposals -= 1
        
    '''
    Lugh coin operations
    '''
    
    def _setWhiteListing(self, proposalId):
        transferParamsRecord = sp.record(address = self.data.proposals[proposalId].t, white = self.data.proposals[proposalId].state)
        c = sp.contract(
                t = sp.TRecord(address = sp.TAddress, white = sp.TBool), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "setWhiteListing"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
    
    def _setLock(self, proposalId):
        transferParamsRecord = sp.record(address = self.data.proposals[proposalId].t, lock = self.data.proposals[proposalId].state)
        c = sp.contract(
                t = sp.TRecord(address = sp.TAddress, lock = sp.TBool), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "setLock"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
    
    def _setPause(self, proposalId):
        transferParamsRecord = sp.record(pause = self.data.proposals[proposalId].state)
        c = sp.contract(
                t = sp.TRecord(pause = sp.TBool), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "setPause"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
    
    def _transfer(self, proposalId):
        transferParamsRecord = sp.record(f = self.data.proposals[proposalId].f, t = self.data.proposals[proposalId].t, amount = self.data.proposals[proposalId].amount)
        c = sp.contract(
                t = sp.TRecord(f = sp.TAddress, t = sp.TAddress, amount = sp.TIntOrNat), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "transfer"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)

if "templates" not in __name__:
    @sp.add_test(name = "AdminMultiSig")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("AdminMultiSig Contract")

        # sp.test_account generates ED25519 key-pairs deterministically:
        firstSigner = sp.test_account("firstSigner")
        secondSigner = sp.test_account("secondSigner")
        thirdSigner = sp.test_account("thirdSigner")
        alice = sp.test_account("Alice")
        bob = sp.test_account("Bob")
        john = sp.test_account("John")
        reserve = sp.test_account("Reserve")
        contractAddr = sp.address("KT1-distantContractToCall-1234")

        # Let's display the accounts:
        scenario.h2("Accounts")
        scenario.show([firstSigner, secondSigner, thirdSigner, alice, bob, reserve])

        c1 = AdminMultiSig(signers = sp.set([firstSigner.address, secondSigner.address, thirdSigner.address]))

        scenario += c1
        
        scenario.h2("#a01 - firstSigner tries to create a setAdministrator proposal")
        scenario += c1.createAddressProposal(proposalId = "aw01", address = alice.address, state = True, operation = "setAdministrator", contractAddr = contractAddr).run(sender = firstSigner, valid = False)
        scenario.h2("#a02 - firstSigner creates a setWhiteListing proposal")
        scenario += c1.createAddressProposal(proposalId = "aw01", address = alice.address, operation = "setWhiteListing", state = True, contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#a03 - firstSigner accepts proposal")
        scenario += c1.accept("aw01").run(sender = firstSigner, valid=False)
        scenario.h2("#a04 - secondSigner rejects proposal")
        scenario += c1.reject("aw01").run(sender = secondSigner)
        scenario.h2("#a05 - thirdSigner accepts proposal")
        scenario += c1.accept("aw01").run(sender = thirdSigner, valid=False)
        scenario.h2("#a06 - thirdSigner tries to create a setLock proposal")
        scenario += c1.createAddressProposal(proposalId = "aw01", address = bob.address, state = False, operation = "setLock", contractAddr = contractAddr).run(sender = thirdSigner, valid = False)
        scenario.h2("#a07 - thirdSigner creates setLock a proposal")
        scenario += c1.createAddressProposal(proposalId = "al01", address = bob.address, state = False, operation = "setLock", contractAddr = contractAddr).run(sender = thirdSigner)
        scenario.h2("#a08 - secondSigner accepts proposal")
        scenario += c1.accept("al01").run(sender = secondSigner)
        scenario.h2("#a09 - firstSigner accepts proposal")
        scenario += c1.accept("al01").run(sender = firstSigner, valid=False)
        scenario.h2("#a10 - firstSigner creates a transfer proposal")
        scenario += c1.createTransferProposal(proposalId = "at01", f = alice.address, t = bob.address, amount = 10000, contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#a11 - secondSigner rejects proposal")
        scenario += c1.reject("at01").run(sender = secondSigner)
        scenario.h2("#a12 - firstSigner rejects proposal")
        scenario += c1.reject("at01").run(sender = firstSigner, valid=False)
        scenario.h2("#a13 - firstSigner creates a transfer proposal")
        scenario += c1.createTransferProposal(proposalId = "at02", f = bob.address, t = reserve.address, amount = 5000, contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#a14 - secondSigner accepts proposal")
        scenario += c1.accept("at02").run(sender = secondSigner)
        scenario.h2("#a15 - secondSigner creates a pause proposal")
        scenario += c1.createPauseProposal(proposalId = "ap01", state = True, contractAddr = contractAddr).run(sender = secondSigner)
        scenario.h2("#a16 - thirdSigner accepts proposal")
        scenario += c1.accept("ap01").run(sender = thirdSigner)
        scenario.h2("#a17 - firstSigner creates a transfer proposal for not referenced address")
        scenario += c1.createTransferProposal(proposalId = "at03", f = john.address, t = reserve.address, amount = 5000, contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#a18 - secondSigner accepts proposal")
        scenario += c1.accept("at03").run(sender = secondSigner)
        scenario.h2("#a19 - firstSigner creates an over transfer proposal for Bob")
        scenario += c1.createTransferProposal(proposalId = "at04", f = bob.address, t = reserve.address, amount = 900000, contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#a20 - secondSigner accepts proposal")
        scenario += c1.accept("at04").run(sender = secondSigner)
        scenario.h2("#a21 - firstSigner a transfer proposal with negative amount")
        scenario += c1.createTransferProposal(proposalId = "neg01", f = john.address, t = reserve.address, amount = -5000, contractAddr = contractAddr).run(sender = firstSigner, valid = False)
