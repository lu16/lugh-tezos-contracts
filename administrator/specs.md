# Multisig administrator

Multi signature "setWhiteListing, setLock, setPause & transfer" with acceptation min. 2 signers.

## INIT

signers address set 


## STORAGE

signers set / addresses bigmap / pauses bigmap / transfers bigmap / authOps set ("setWhiteListing", "setLock")

## ENTRYPOINTS

| ENTRYPOINT | PARAMS | PERMISSIONS | CONDITIONS |
| ------ | ------ | ------ | ------ |
| createAddressProposal | proposalId: string, operation: string, address: address, contractAddr: address, state: bool | signer | proposalId does not exist |
| createPauseProposal | proposalId: string, contractAddr: address, state: bool | signer | proposalId does not exist |
 createTransferProposal | proposalId: string, contractAddr: address, f: address, t: address, amount: int | signer | proposalId does not exist |
| accept | string | signer | proposalId exists and not closed, and not already accepted by caller |
| reject | string | signer | proposalId exists and not closed, and not already rejected by caller |

## ERROR CODE

| ERROR CODE | MESSAGE                        |
|------------|--------------------------------|
| #01        | Unreferenced signing address   |
| #02        | Proposal Id already referenced |
| #03        | Proposal Id not referenced     |
| #04        | Open proposals limit reached   |
| #05        | Proposal Id is closed          |
| #06        | Operation not permitted        |
| #07        | Transfer amount not permitted  |
| #08        | Proposal already accepted      |
